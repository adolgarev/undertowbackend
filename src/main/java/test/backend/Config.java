package test.backend;

public class Config {

	public String getHost() {
		return "localhost";
	}

	public Integer getPort() {
		return 9999;
	}

	public Integer getTaskThreadPoolSize() {
		return 30;
	}
}
