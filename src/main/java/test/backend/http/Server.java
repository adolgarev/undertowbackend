package test.backend.http;

import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import test.backend.Config;

public class Server {

	private final Config conf;

	public Server(Config conf) {
		this.conf = conf;
	}

	public void run() throws Exception {
		Undertow server = Undertow.builder()
				.addHttpListener(conf.getPort(), conf.getHost())
				.setWorkerOption(org.xnio.Options.WORKER_TASK_CORE_THREADS, conf.getTaskThreadPoolSize())
				.setHandler(new HttpHandler() {
					@Override
					public void handleRequest(final HttpServerExchange exchange) throws Exception {
						if (exchange.isInIoThread()) {
							exchange.dispatch(this);
							return;
						}
						exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
						exchange.getResponseSender().send("Ok");
					}
				}).build();
		server.start();
	}

	public static void main(String[] args) throws Exception {
		new Server(new Config()).run();
	}
}
